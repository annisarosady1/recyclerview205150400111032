package com.example.recyclerview205150400111032;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.recyclerview205150400111032.adapter.MahasiswaAdapter;
import com.example.recyclerview205150400111032.model.Mahasiswa;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnMahasiswaListener {

    RecyclerView recyclerView;
    MahasiswaAdapter mahasiswaRecyclerAdapter;
    ArrayList<Mahasiswa> _mahasiswaList;
    Button bt1;
    EditText inputNama, inputNIM;
    int[] foto = {R.drawable.avatar};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputNama = findViewById(R.id.input_nama_edit_text);
        inputNIM = findViewById(R.id.input_NIM_edit_text);

        bt1 = findViewById(R.id.bt1);
        bt1.setOnClickListener(this);
        loadData();
        initRecyclerView();
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.rvMahasiswa);
        mahasiswaRecyclerAdapter = new MahasiswaAdapter(_mahasiswaList, this, this);
        recyclerView.setAdapter(mahasiswaRecyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void loadData() {
        _mahasiswaList = new ArrayList<>();
        _mahasiswaList.add(new Mahasiswa("Nabila", "205150400111099", foto[0]));
        _mahasiswaList.add(new Mahasiswa("Ahmad", "205150400111098", foto[1]));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == bt1.getId())
        {
            String nama = inputNama.getText().toString();
            String NIM = inputNIM.getText().toString();

            if(nama.equals("") || NIM.equals(""))
            {
                Toast.makeText(MainActivity.this,
                        "Nama dan NIM Tidak Boleh Kosong!", Toast.LENGTH_LONG).show();
                return;
            }

            inputNama.setText("");
            inputNIM.setText("");
            mahasiswaRecyclerAdapter.notifyDataSetChanged();
        }
        hideKeyboard((Button)v);
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }catch(Exception ignored) {
        }
    }

    @Override
    public void onMahasiswaClick(int position) {
        Log.d(TAG, "onMahasiswaClick: clicked.");

        Intent intent = new Intent(this, BiodataActivity.class);
        intent.putExtra("keyNama", _mahasiswaList.get(position).get_nama());
        intent.putExtra("keyNIM", _mahasiswaList.get(position).get_NIM());
        intent.putExtra("keyFoto", _mahasiswaList.get(position).get_foto());
        startActivity(intent);
    }
}