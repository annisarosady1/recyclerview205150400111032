package com.example.recyclerview205150400111032.model;

public class Mahasiswa {

    String _nama, _NIM;
    int _foto;

    public Mahasiswa(String _nama, String _NIM, int _foto) {
        this._nama = _nama;
        this._NIM = _NIM;
        this._foto = _foto;
    }

    public String get_nama() {
        return _nama;
    }

    public String get_NIM() {
        return _NIM;
    }

    public int get_foto() {
        return _foto;
    }
}
